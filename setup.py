# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    mit_license = f.read()

setup(
    name='wibc',
    version='0.2a',
    description='Website Image-Based Classification',
    long_description=readme,
    author='Anton Akusok',
    author_email='anton.akusok@arcada.fi',
    url='https://github.com/akusok',
    license=mit_license,
    include_package_data = True,
    install_requires=['joblib', 'pillow', 'numpy',
                      'scikit-image', 'scikit-learn',
                      'mxnet','blosc>=1.5.1','opencv-python',
                      'jupyter', 'lassie', 'lxml', 'pebble', 'requests'],
    packages=find_packages(exclude=('tests', 'docs'))
)

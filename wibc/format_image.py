from __future__ import division
import warnings
import numpy as np
from PIL import Image, ImageFile
import requests
from io import BytesIO
ImageFile.LOAD_TRUNCATED_IMAGES = True

# Use to avoid some problems with webpages that don't like robots.
user_agent = ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 '
              '(KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36')
hdr = {'User-Agent': user_agent}

# turn off "numeric conversion warning" for image scaling
# warnings.simplefilter("ignore", UserWarning, lineno=110)
warnings.filterwarnings("ignore", message="Palette images with Transparency   expressed in bytes")


def format_image(fimg, verbose=False):
    """Loads and formats an image, handles all possible errors.

    Smallest image is at least 10 pixels in each dimension, and 100 pixels total.
    Largest image is 1024 pixels, resized if more than that.

    :param fimg: 'image' file name
    :param verbose: whether to print detailed information
    :return: image object in RGB format, or None
    """

    image = None

    # load image from local file
    try:
        image = Image.open(fimg)
    except FileNotFoundError:
        pass
    except (OSError, ValueError):
        if verbose: print("error opening image file")
        return None

    # load image from web
    if image is None:
        try:
            s = requests.Session()
            response = s.get(fimg, headers=hdr, timeout=10, verify=True)
        except requests.exceptions.RequestException:
            if verbose: print("request exception")
            return None

        if not response.ok:
            if verbose: print("Response code {}".format(response.status_code))
            return None

        if len(response.content) < 10:
            if verbose: print("Too short or empty response: {}".format(response.content))
            return None

        if b"html" in response.content[:20]:
            if verbose: print("Response not an image: got a webpage instead")
            return None

        try:
            image = Image.open(BytesIO(response.content))
        except OSError:
            if verbose:
                print("Response not a valid image")
                print(response.content[:1000])
            return None

    # successfully obtained a web image
    if verbose: print("processing image at: {}".format(fimg))

    # convert image to correct format
    try:
        image = image.convert("RGB")
    except (OSError, SyntaxError):
        if verbose: print("error converting to RGB")
        return None

    # ignore tiny images and lines
    s = image.size
    if min(s) < 10 or s[0]*s[1] < 100:
        if verbose: print("image too small")
        return None

    # adjust size
    factor = max(image.size) / 1024
    if factor > 1:
        newsize = [int(np.floor(i/factor)) for i in image.size]
        if min(newsize) < 5:
            if verbose: print("image too small")
            return None
        image = image.resize(newsize, Image.LANCZOS)

    return image

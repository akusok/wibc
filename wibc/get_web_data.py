import lassie
import lxml.html
from urllib import request
import requests
from functools import partial
from .format_image import format_image, hdr
from pebble import ProcessPool, ProcessExpired
from concurrent.futures import TimeoutError


def myget(url):
    # gets a list of image urls
    ls = lassie.Lassie()
    ls.request_opts = {'timeout': 30, 'headers': hdr}

    url = url.rstrip('/')
    if not url.startswith('http'):
        url = 'http://' + url

    try:
        data = ls.fetch(url, all_images=True)
        for item in data['images']:
            yield item['src']

    except Exception as e:
        print(e, url)


def mysave(params):
    # saves image url into a file

    img_url, fout = params

    if ".png" in img_url:
        fout += ".png"
    elif ".gif" in img_url:
        fout += ".gif"
    else:
        fout += ".jpg"

    try:
        request.urlretrieve(img_url, fout)
    except Exception as e:
        pass
        # print(e, img_url)


def myget_sub_urls(url):
    """Obtain all sublinks from a given url.

    Currently only works in sequential code.
    """

    url = url.rstrip('/')
    if not url.startswith('http'):
        url = 'http://' + url
    yield from myget(url)

    s = requests.Session()
    connection = s.get(url, headers=hdr, timeout=30)
    dom = lxml.html.fromstring(connection.text)

    try:
        for link in dom.xpath('//a/@href'):
            if link.startswith("//"):
                yield from myget('http:' + link.rstrip('/'))

            elif link.startswith("/", 0) and not link.startswith("/", 1):
                yield from myget(url + link.rstrip('/'))

            elif not link.startswith(('/', 'http', url, 'www')):
                yield from myget(url + '/' + link.rstrip('/'))

            elif link.startswith("http"):
                yield from myget(link.rstrip('/'))

    except Exception:
        pass


def myget_sub_urls_unique(url):
    """Obtain all sublinks from a given url.

    Currently only works in sequential code.
    """
    found_items = set()  # for detecting duplicates

    url = url.rstrip('/')
    if not url.startswith('http'):
        url = 'http://' + url
    for item in myget(url):
        found_items.add(item)
        yield item

    s = requests.Session()
    connection = s.get(url, headers=hdr, timeout=30)
    dom = lxml.html.fromstring(connection.text)

    try:
        for link in dom.xpath('//a/@href'):
            if link.startswith("//"):
                for item in myget('http:' + link.rstrip('/')):
                    if item not in found_items:
                        found_items.add(item)
                        yield item

            elif link.startswith("/", 0) and not link.startswith("/", 1):
                for item in myget(url + link.rstrip('/')):
                    if item not in found_items:
                        found_items.add(item)
                        yield item

            elif not link.startswith(('/', 'http', url, 'www')):
                for item in myget(url + '/' + link.rstrip('/')):
                    if item not in found_items:
                        found_items.add(item)
                        yield item

            elif link.startswith("http"):
                for item in myget(link.rstrip('/')):
                    if item not in found_items:
                        found_items.add(item)
                        yield item

    except Exception:
        pass


def get_all_imgs(url, sub_urls=False, n_jobs=10, img_max=10, verbose=True):
    """ This function allows you to obtain all images for a given url with the option
        to scrap all the images from the sub urls too.

        :param url: webpage address to grab images from
        :param sub_urls: whether to follow sub-links, default is False. Sub-links are
                         followed automatically if not enough images are found on main page.
        :param n_jobs: number of parallel jobs for downloading, None for sequential
        :param img_max: number of images to obtain before stopping
        :param verbose: whether to show some statistics
    """

    if sub_urls:
        generator = myget_sub_urls_unique(url)
    else:
        generator = myget(url)

    if img_max is None:
        img_max = float("+inf")

    images = []
    if n_jobs is None or sub_urls:  # sequential processing
        for url1 in generator:
            img1 = format_image(url1, verbose=verbose)
            if img1 is not None:
                images.append(img1)
            if len(images) >= img_max:
                break
        return images

    else:
        # fix for parallel part failing without a sequential call first
        image = format_image(next(generator), verbose=verbose)
        if image is not None:
            images.append(image)

        # Python parallel processing with cut-off and timeout
        with ProcessPool(max_workers=n_jobs, max_tasks=10) as pool:
            # with 'myget_sub_urls', next line does not return immediately -- and early stopping does not work either
            future = pool.map(partial(format_image, verbose=verbose), generator, chunksize=1, timeout=10)
            results = future.result()

            # process pool results
            while True:
                try:
                    image = next(results)
                    if image is not None:
                        images.append(image)
                except StopIteration:
                    break
                except TimeoutError as error:
                    if verbose: print("function took longer than %d seconds" % error.args[1])
                except ProcessExpired as error:
                    if verbose: print("%s. Exit code: %d" % (error, error.exitcode))
                except Exception as error:
                    if verbose: print("function raised %s" % error)
                    if verbose: print(error.traceback)  # Python's traceback of remote process

                if len(images) >= img_max:
                    break

            pool.stop()
            pool.join()

        # # Same with Multiprocessing, but this fails for repetitive calls for weird reasons
        # p = Pool(processes=n_jobs)
        # results = p.imap(partial(format_image, verbose=verbose), generator, chunksize=1)
        # images = []
        #
        # while True:
        #     try:
        #         image = results.next(timeout=1)
        #         if image is not None:
        #             images.append(image)
        #     except StopIteration:
        #         break
        #     except TimeoutError:
        #         pass
        #
        #     if len(images) >= img_max:
        #         break
        #
        # p.terminate()


    return images

import pickle
import pkg_resources
import mxnet as mx
import numpy as np
from .deepfeatures import extract_inception21k
from .get_web_data import get_all_imgs
from .format_image import format_image



class WIBC_weapon(object):

    def __init__(self, img_max=10, img_min=1, sub_urls=False, n_jobs=10, use_gpu=False, verbose=False):
        """Website image-based classifier for weapons class.

        :param img_max: maximum amount of images to analyze in a single website (randomly selected)
        :param img_min: minimum amount of images to provide prediction for a website
        :param sub_urls: whether to search sub-urls for more images
        :param n_jobs: number of downloading jobs
        :param use_gpu: whether to use GPU to run MXNet deep network model
        :param verbose: print debug information
        """
        self.img_max = img_max
        self.img_min = img_min
        self.sub_urls = sub_urls
        self.n_jobs = n_jobs
        self.verbose = verbose

        # load DNN model
        dnn_model = 'data/Inception21k'
        epoch = 9
        data_path = pkg_resources.resource_filename('wibc', dnn_model)
        sym, arg_params, aux_params = mx.model.load_checkpoint(data_path, epoch)

        if use_gpu:
            mod21k = mx.mod.Module(symbol=sym, context=mx.gpu())
        else:
            mod21k = mx.mod.Module(symbol=sym, context=mx.cpu())

        mod21k.bind(for_training=False, data_shapes=[('data', (1, 3, 224, 224))])
        mod21k.set_params(arg_params, aux_params, allow_missing=True)
        self.mod21k = mod21k

        # load Random Forest model
        rf_model = 'data/randomforest_weapon.pkl'
        model_filename = pkg_resources.resource_filename('wibc', rf_model)
        self.mod_rf = pickle.load(open(model_filename, 'rb'))

    def run(self, url):
        images = get_all_imgs(url, img_max=self.img_max, n_jobs=self.n_jobs, sub_urls=self.sub_urls, verbose=self.verbose)

        # not enough images, search for more in sub-urls
        if len(images) < self.img_min and not self.sub_urls:
            images = get_all_imgs(url, img_max=self.img_max, n_jobs=self.n_jobs, sub_urls=True, verbose=self.verbose)

        # not enough images, return None
        if len(images) < self.img_min:
            return None

        features = np.vstack([extract_inception21k(img, self.mod21k) for img in images])
        prob = self.mod_rf.predict_proba(features)
        return prob[:, 1]

    def run_file(self, fname):
        try:
            img = format_image(fname)
        except IOError:
            return None

        if img is None:
            return None

        features = extract_inception21k(img, self.mod21k)
        prob = self.mod_rf.predict_proba(features)
        return prob[:, 1]

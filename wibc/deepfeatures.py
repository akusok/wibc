import numpy as np
import mxnet as mx
from scipy.misc import imread, imresize
from collections import namedtuple
Batch = namedtuple('Batch', ['data'])
from PIL import Image


def extract_inception21k(imgfile, mod21k):

    # load and resize image
    try:
        if isinstance(imgfile, Image.Image):
            img = imgfile
        else:
            img = imread(imgfile)
    except AttributeError:
        return None

    if img is None:
        return None

    # convert into format (batch, RGB, width, height)
    img = imresize(img, (224, 224), interp='lanczos')
    img = np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 1, 2)
    img = img[np.newaxis, :]

    # Inception21k specific normalization
    img = img.astype(np.float32) - 117

    # run the network
    mod21k.forward(Batch([mx.nd.array(img)]))
    feats = mod21k.get_outputs()[0].asnumpy()

    return feats

"""Feature extractor from images of given URLs.

Grabs images from each of the given URLs, extracts their features with a pre-trained DL network,
and returns the extracted features.

Now with Scikit-Learn transformer interface.

DL weights bundled in the package, original link:
https://github.com/dmlc/mxnet-model-gallery/blob/master/imagenet-21k-inception.md

"""

# Authors: Anton Akusok <anton.akusok@arcada.fi>
#          Leonardo Espinosa Leal <leonardo.espinosaleal@arcada.fi>
#
# License: MIT

import numpy as np
import mxnet as mx
import pkg_resources
from .deepfeatures import extract_inception21k
from .get_web_data import get_all_imgs
from sklearn.base import BaseEstimator, TransformerMixin

__all__ = ["DLFeatures"]

class DLFeatures(BaseEstimator, TransformerMixin):
    """Extract features with Inception21k Deep Learning model.

    :param img_min: minimum images per website
    :param img_max: maximum images per website, random order
    :param img_jobs: parallel jobs to download images from a website
    :param sub_urls: whether to check sub-urls
    :param dnn_model: path to Inception21k model file
    :param use_gpu: whether to use GPU in MXNet
    :param verbose: debug output
    """

    def __init__(self, img_min=1, img_max=10, img_jobs=10, sub_urls=False,
                 dnn_model='data/Inception21k', use_gpu=False, verbose=False):
        self.img_max = img_max
        self.img_min = img_min
        self.img_jobs = img_jobs
        self.sub_urls = sub_urls
        self.dnn_model = dnn_model
        self.use_gpu = use_gpu
        self.verbose = verbose

    def fit_transform(self, X, y=None, **fit_params):
        """Extracts deep learning features from images.

        :param X: a list of URLs
        :param y: unused
        :param fit_params: no params
        :return: list of features with one element per URL in X, or None for unavailable features
        """

        # initialization
        try:
            getattr(self, "model_")
        except AttributeError:
            # initialize missing model
            data_path = pkg_resources.resource_filename('wibc', self.dnn_model)
            sym, arg_params, aux_params = mx.model.load_checkpoint(data_path, epoch=9)

            if self.use_gpu:
                mod21k = mx.mod.Module(symbol=sym, context=mx.gpu())
            else:
                mod21k = mx.mod.Module(symbol=sym, context=mx.cpu())

            mod21k.bind(for_training=False, data_shapes=[('data', (1, 3, 224, 224))])
            mod21k.set_params(arg_params, aux_params, allow_missing=True)
            self.model_ = mod21k

        # get images from urls
        if isinstance(X, str):
            raise TypeError("Input should be a list of URLs")
        if y is None:
            y = [None for _ in X]  # a list of None for correct zip(X, y)

        features = []
        outputs = []
        for url, y1 in zip(X, y):
            images = get_all_imgs(url, img_max=self.img_max, n_jobs=self.img_jobs,
                                  sub_urls=self.sub_urls, verbose=self.verbose)

            # not enough images, search for more in sub-urls
            if len(images) < self.img_min and not self.sub_urls:
                images = get_all_imgs(url, img_max=self.img_max, n_jobs=self.img_jobs,
                                      sub_urls=True, verbose=self.verbose)

            if len(images) >= self.img_min:
                feats1 = np.vstack([extract_inception21k(img, self.model_) for img in images])
                features.append(feats1)
                outputs.append([y1] * feats1.shape[0])

        features = np.vstack(features)
        outputs = np.stack(outputs)
        return features, outputs

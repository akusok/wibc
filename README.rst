Website Image-Based Classification
============

CloSer project of image-based classification of websites, aims towards offensive class detection from a full-page
rendering. Under development.


Installation:
~~~~~~~~~~~~

Download repository, ``cd`` into it and run ``pip install -e .``


Usage:
~~~~~~

See ``bin/run_wibc_weapons.ipynb``

For Scikit-Learn like interface: ``bin/test_sklearn_interface.ipynb``
